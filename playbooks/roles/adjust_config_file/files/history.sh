#!/bin/bash
export HISTCONTROL=
export HISTFILE=$HOME/.bash_history
export HISTFILESIZE=2000
export HISTIGNORE=
export HISTSIZE=1000
export HISTTIMEFORMAT="%Y-%m-%d %T %z " 

typeset -r HISTCONTROL
typeset -r HISTFILE
typeset -r HISTFILESIZE
typeset -r HISTIGNORE

if [ $(readonly -p|grep "HISTSIZE") ]
        then typeset -r HISTSIZE
fi

if [ $(readonly -p|grep "HISTTIMEFORMAT") ] 
        then typeset -r HISTTIMEFORMAT
fi

shopt -s cmdhist
shopt -s histappend

PROMPT_COMMAND=$(history -a) 
if [ $(readonly -p|grep "PROMPT_COMMAND") ]
        then typeset -r PROMPT_COMMAND 
fi

function log2syslog
{
        declare command
        ORIGEM=$(echo $SSH_CLIENT | awk '{print $1}')
        command="$ORIGEM: $BASH_COMMAND"
	if [ "$BASH_COMMAND" != "$PROMPT_COMMAND" ]
        	then logger -p local1.notice -t bash -i -- $USER : $command
	fi

}
#trap log2syslog DEBUG

