#!/bin/bash


#Script para monitoração proativa das maquinas.
#Leandro Gomes Diez 16/12/2016
#Raphael Moreno/Charles Dias  10/05/2017
#Versao

#1.0 16/12/16 - Versao Inicial
#1.1 10/05/17 - Realizado ajuste de coleta de filesystem / consumo de recursos

######VARIAVEIS GLOBAIS
#Variavel Global Cluster
HA_BIN="/opt/VRTS/bin/hastatus"

#ALERTA DE FILESYSTEMS
ALERT="95"
WHO=`whoami`
# Separe os filesystems com |
# exemplo: EXCLUDE_LIST="/dev/hdd1|/dev/hdc5"
EXCLUDE_LIST=""




################-----FUNCOES----###############################################
cluster_node_status() {

if [ -e "$HA_BIN" ];then
STATUS=`$HA_BIN -sum|awk '$1 == "A"' |grep -v RUNNING`
if [ $? = "0" ];then
echo "Cluster:"
echo -e "\033[0;31m"Existem nos do cluster com status diferente de "RUNNING"" \033[0m"
echo $STATUS
echo ""
else
echo "Cluster:"
echo "Todos os nós do cluster estão com status running"
fi

STATUS=`/opt/VRTS/bin/lltstat -nvv active|grep DOWN`
if [ $? = "0" ];then
echo -e "\033[0;31m"Existem links de heartbeat com problemas!" \033[0m"
echo $STATUS
echo ""
fi

STATUS=`$HA_BIN -sum|awk '$1' |grep C`
if [ $? = "0" ];then
echo -e "\033[0;31m"Existem recursos em FREEZE no cluster" \033[0m"
echo $STATUS
echo ""
fi

STATUS=`$HA_BIN -sum|awk '$1' |grep H`
if [ $? = "0" ];then
echo -e " \033[0;31m"Existem recursos em DISABLE no cluster" \033[0m"
echo $STATUS
echo ""
fi


fi
}


resource_status() {
if [ -e "$HA_BIN" ];then
STATUS=`$HA_BIN -sum|awk '$1 == "B"' |grep FAULTED`
if [ $? = "0" ];then
echo -e "\033[0;31m"Existem recursoss em faulted" \033[0m"
echo $STATUS
#else
#echo "Nao existem recursos em faulted"
fi
fi
}


filesystem() {
function main_prog() {
while read output;
do
#echo $output
  usep=$(echo $output | awk '{ print $1}' | cut -d'%' -f1)
  partition=$(echo $output | awk '{print $2}')

  if [ $usep -ge $ALERT ] ; then
             echo "Filesystem com alta utilizacao \"$partition ($usep%)\" no servidor $(hostname), $(date)"
  fi
done
}
if [ "$EXCLUDE_LIST" != "" ] ; then
  df -lhP |  grep -vE "^[^/]|tmpfs|cdrom|${EXCLUDE_LIST}" | awk '{print $5 " " $6}' | main_prog
else
  df -lhP |  grep -vE "^[^/]|tmpfs|cdrom"| awk '{print $5 " " $6}' | main_prog
fi

}


linux() {

ROOT_PERC=`df -lhP /| column -t|grep -vi filesystem |awk '{print $5}'`
ROOT_TOT=`df -lhP /| column -t|grep -vi filesystem |awk '{print $2}'`
HOST=`hostname`
PROC_TOT=`ps -ef|wc -l`
MEM_TOT=`free -m|grep -i mem|awk '{print $2}'`
#MEM_CACHE=`free -m|grep cache|grep -v tot|awk '{print $4}'`
MEM_CACHE=`free -m|grep Mem | awk '{print $7}'`
MEM_FREE=`echo "($MEM_CACHE/$MEM_TOT)"*100|bc -l|cut -d. -f1`%
SWAP_TOT=`free -m|grep -i swap|awk '{print $2}'`
SWAP_USO=`free -m|grep -i swap|awk '{print $3}'`
SWAP_FREE=`echo "($SWAP_USO/$SWAP_TOT)"*100|bc -l|cut -d. -f1`
if [ -z "$SWAP_FREE" ];then
        SWAP_FREE=0%
fi
LOAD=`uptime | cut -d'l' -f2 | awk '{print $3}' | cut -d, -f1`
CPU_IDLE=`sar 1 1|grep -i average|awk '{print $8}'|cut -d. -f1`%
UPTIME=`uptime|awk '{print $3" "$4}'| cut -d, -f1`
}

printa(){
echo ""
echo "Hostname: $HOST"
echo ""
echo "Uso do /:      "$ROOT_PERC" de $ROOT_TOT"
echo "Processos:     "$PROC_TOT""
echo "Memoria Livre: "$MEM_FREE""
echo "%SWAP em uso:  "$SWAP_FREE""
echo "Load Average:  "$LOAD""
echo "%CPU Idle:     "$CPU_IDLE""
echo "Uptime:        "$UPTIME""

}

uptime_check(){
echo "
uptime:"
uptime
}



# ATIVA VERIFICAÇÕES
if [ $WHO = "root" ];then

echo "
Checklist Unix:"
#uptime_check
linux
printa
echo ""
cluster_node_status
resource_status
#lltstat
echo "
Filesystems com utilizacao acima de 95%:"
filesystem
echo " "
fi