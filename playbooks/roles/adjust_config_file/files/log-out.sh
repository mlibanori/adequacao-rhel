#!/bin/bash
# Log out in 5 minutes if the session is idle
export TMOUT=900
readonly TMOUT